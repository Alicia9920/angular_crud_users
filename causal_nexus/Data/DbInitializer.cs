﻿using causal_nexus.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace causal_nexus.Data
{
    public static class DbInitializer
    {
        public static void Initialize(ApplicationContext context)
        {
            context.Database.EnsureCreated();

            if(!context.Users.Any()){
                // seeded user for demo
                context.Users.Add(new User
                {
                    Name= "Alicia",
                    Surname = "Hlatshwayo",
                    IDNumber= "960215 0008 084",
                    PassportNumber= "A19097676",
                    ContactDetails = new ContactDetails
                    {
                        EmailAddress = "charmainalicia@gmail.com",
                        MobileNumber = "084 220 9920"
                    }
                });
                context.SaveChanges();
            }
        }
    }
}
