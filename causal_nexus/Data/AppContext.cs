﻿using causal_nexus.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace causal_nexus.Data
{
    public class ApplicationContext : DbContext
    {


        public static IConfiguration Configuration { get; set; }
        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options)
        {
        }
        public ApplicationContext()
        {
        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
        }
        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            var bd = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");
            Configuration = bd.Build();
            builder.UseSqlServer($"{Configuration["ConnectionString:CausalNexus"]}");
            base.OnConfiguring(builder);
        }
        public DbSet<User> Users { get; set; }
    }
}
