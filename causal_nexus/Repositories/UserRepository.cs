﻿using causal_nexus.Data;
using causal_nexus.Models;
using causal_nexus_backend.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace causal_nexus.Repositories
{
    public class UserRepository : IRepository<User, long>
    {

        public ApplicationContext context { get; set; }
        public UserRepository()
        {
            context = new ApplicationContext();
        }
        public IEnumerable<User> Get()
        {
            return context.Users.ToList();
        }

        public User Get(long id)
        {
            return context.Users
                .Include(u => u.ContactDetails)
                .SingleOrDefault(x => x.UserID == id);
        }

        public void Add(User entity)
        {
            context.Users.Add(entity);
            context.SaveChanges();
        }

        public void Remove(long id)
        {
            var obj = context.Users.Find(id);
            context.Users.Remove(obj);
            context.SaveChanges();
        }

        public void Update(User entity)
        {
            context.Entry(entity).State = EntityState.Modified;
            context.SaveChangesAsync();
        }
    }
}
