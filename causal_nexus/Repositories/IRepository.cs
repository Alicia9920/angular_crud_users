﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace causal_nexus_backend.Repositories
{
    public interface IRepository<T, in TPk> where T : class
    {
        IEnumerable<T> Get();
        T Get(TPk id);
        void Update(T entity);
        void Add(T entity);
        void Remove(TPk id);
    }
}
