import { Injectable } from '@angular/core';

export class User {
  userID?: string;
  name?: string;
  surname?: string;
  idNumber?: string;
  passportNumber?: string;
  contactDetails: ContactDetails;
}

export class ContactDetails {
  ContactDetailsID?: string;
  MobileNumber?: string;
  EmailAddress?: string;
}


