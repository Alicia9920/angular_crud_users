import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { UserService } from '../_services/user.service';
import { User } from '../models/User';
import { NotifierService } from 'angular-notifier';

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.css']
})
export class UpdateUserComponent implements OnInit {
  @Input() user: User;
  private readonly notifier: NotifierService;
  constructor(
    notifierService: NotifierService,
    private userService: UserService
  ) {
    this.notifier = notifierService;
  }
  ngOnInit() {}
  onClickSubmit() {
    this.userService.updateUser(this.user).subscribe(() => {
      this.notifier.notify('success', 'Update was succesful!');
      document.getElementById('close').click();
    });
  }
}
