import { Component } from '@angular/core';
import { User, ContactDetails } from '../models/User';
import { UserService } from '../_services/user.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
/** user-profile component*/
export class UserProfileComponent {
  public closeModal: Function;

  /** user-profile ctor */
  public user = {
    contactDetails: {}
  } as User;

  constructor(private userService: UserService) {}
  public ngOnInit() {
    this.getUserDetails();
  }
  getUserDetails() {
    this.userService.getProfile().subscribe(data => (this.user = data));
  }

}
