﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using causal_nexus.Data;
using causal_nexus.Models;
using causal_nexus.Repositories;
using causal_nexus_backend.Repositories;

namespace causal_nexus.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private IRepository<User,long> userRepository;

        public UsersController()
        {
            this.userRepository = new UserRepository();
        }

        // GET: api/Users/profile
        [HttpGet("profile")]
        public IActionResult GetUserProfile()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            // getting default seeded user for demo purposes
            // If system had authentication, getUser by ID would be more appropriate 
            var user = userRepository.Get(1);

            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }


        // PUT: api/Users/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUser([FromRoute] long id, [FromBody] User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != user.UserID)
            {
                return BadRequest();
            }

            

            try
            {
                userRepository.Update(user);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }


        private bool UserExists(long id)
        {
            return userRepository.Get(id) == null ? false : true;
        }
    }
}